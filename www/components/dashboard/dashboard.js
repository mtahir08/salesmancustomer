(function () {
  angular.module('myApp')
    .controller('dashboardController', function ($scope, $http, $q, authService, $rootScope, $state, $ionicActionSheet, $ionicPopup, orderService, $firebaseArray, $firebaseObject, $timeout, $ionicSideMenuDelegate) {
      $scope.token = authService.getIsToken();
      $scope.loading = false;
      $scope.items = [];
      orderService.syncSalesmanObj.$loaded().then(function (data) {
        $scope.temp = data.retailers;
      });
      orderService.syncSalesmanRetailersArr.$loaded().then(function (data) {
        $scope.items = data;
      });
      $state.go('dashboard.retailers');
      $scope.toggleLeft = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };
      $scope.addRetailer = function () {
        $state.go("addRetailer");
      };
      $scope.logout = function () {
        var obj = {};
        $scope.loading = true;
        authService.logOut(obj, $q)
          .then(function (res) {
            $scope.loading = false;
            $state.go("login");
            $scope.token = false;
          }, function (error) {
            console.log("in error")
          });
      };
      //         $scope.data = {
      //   showDelete: false
      // };

      // $scope.edit = function(item) {
      //   alert('Edit Item: ' + item.id);
      // };
      // $scope.share = function(item) {
      //   alert('Share Item: ' + item.id);
      // };

      // $scope.moveItem = function(item, fromIndex, toIndex) {
      //   $scope.items.splice(fromIndex, 1);
      //   $scope.items.splice(toIndex, 0, item);
      // };

      $scope.onItemDelete = function (item) {
        $scope.items.splice($scope.items.indexOf(item), 1);
      };

      $scope.showActionsheet = function (item, itemIndex) {
        console.log(itemIndex);
        $ionicActionSheet.show({
          titleText: 'Action Options',
          buttons: [
            {text: '<i class="icon ion-ios-cart"></i> Place Order'},
            {text: '<i class="icon ion-more"></i> Customer Details'}
          ],
          destructiveText: '<i class="icon ion-trash-a"></i> Delete',
          cancelText: '<i class="icon"></i> Cencel',
          cancel: function () {
            console.log('CANCELLED');
          },

          buttonClicked: function (index) {
            if (index === 0) {
              console.log('Place Order', index);
              orderService.setRetailer(item);
              console.log(item);
              $state.go('order');
            }
            else {
                $state.go('dashboard.retailer', { id: item.$id});
              console.log('Customer Detail', index);
              console.log('Customer Detail', item);
            }
            return true;
          },
          destructiveButtonClicked: function () {
            console.log('BUTTON CLICKED');

            var confirmPopup = $ionicPopup.confirm({
              title: 'Delete',
              template: 'Are you sure you want to delete this user?'
            });

            confirmPopup.then(function (res) {
              if (res) {
                console.log(item);
                // syncObj.splice(syncArr.indexOf(item), 1);
                var temp = item.$id;
                ref.child(temp).remove(function (error) {
                  if (error)
                    console.log("err", err);
                  console.log("res")
                });
                /*              syncArr.$remove(item).then(function(ref) {
                 ref.key() === item.$id; // true
                 console.log('You are sure',(ref.key() === item.$id));
                 $scope.items = syncArr;
                 });*/

                console.log('You are sure', temp);
              } else {
                console.log('You are not sure');
              }
            });
            // function(index) {

            // A confirm dialog


            return true;
          }
        });
      };


      /**
       *      salesman orders start
       * */
      orderService.syncSalesmanOrdersArr.$loaded().then(function (data) {
        $scope.salesmanOrders = data;
      });

      /**
       *      salesman Retailers start
       * */



    })
})();
