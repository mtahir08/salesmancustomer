(function () {
  angular.module('myApp')
    .controller('forgotPasswordController', ForgotPasswordController);
  function ForgotPasswordController($scope, authService, $state) {
    $scope.loading = false;
    $scope.forgotPass = {
      email: ''
    };
    $scope.sendEmail = function () {
      $scope.loading = true;
      authService.forgotPassword($scope.forgotPass)
        .then(function (responseData) {
          console.log('responseData', responseData);
          $scope.loading = false;
          $state.go('login');
        },
        function (responseError) {
          console.log('responseError', responseError)
        })
    }
  }
})();
