(function () {
  angular.module('myApp')
    .controller('headerController', function ($scope, $http, $q, $state, $timeout, authService, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };
    })
})()
