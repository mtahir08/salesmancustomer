(function () {
  angular.module('myApp')
    .controller('loginController', LoginController);
  function LoginController($scope, $rootScope, $http, $q, $state, authService) {
    $scope.loading = false;
    var token = localStorage.getItem('token');
    if (token) {
      console.log("already signedin");
      $state.go('dashboard');
    }
    $scope.userLogin = {
      email: 'wow@wow.com',
      password: '123456'
    };

    $scope.login = function () {
      $scope.loading = true;
      if ($scope.userLogin == undefined) {
        console.log("$scope.userLogin == undefined")
      }
      else if (isEmpty($scope.userLogin) == false) {
        console.log("empty object")
      } else {
        $scope.loging = true;
        authService.logIn($scope.userLogin, $q)
          .then(function (responseData) {
            console.log(responseData);
            if (responseData.status) {
              authService.setToken(responseData.authData.token, responseData.authData.uid);
              console.log(responseData);
              $state.go('dashboard');
              $scope.loading = false;

            } else if (responseData.data.path) {
              // authService.setCompany();
              var collection = responseData.data;
              for (var key in collection) {
                sessionStorage.setItem(key, collection[key]);
              }
              $scope.loading = false;
              $state.go('resetPassword');
              // window.location = ('/resetPassword');
            }
            else if (responseData.code === "INVALID_PASSWORD") {
              alert("invalid username or password")

            } else {
              console.log('status false', responseData)
            }
          },
          function (responseError) {
            console.log('responseError', responseError)
          });
      }
    };
    function isEmpty(object) {
      for (var key in object) {
        if (object.hasOwnProperty(key) && object[key] == null) {
          return false;
        }
      }
      return true;
    }
  }
})();
