(function () {
  angular.module('myApp')
    .controller('ordersController', function ($scope, $http, $q, $state, authService, orderService, $ionicSideMenuDelegate) {
      $scope.toggleLeft = function () {
        $ionicSideMenuDelegate.toggleLeft();
      };
    });
})();
