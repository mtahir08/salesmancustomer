(function () {
  angular.module('myApp')
    .controller('addRetailerController', function ($scope, $http, $q, $state, authService, $rootScope, $ionicActionSheet, $ionicPopup, orderService) {
      $scope.addUserForm = {
        name: '',
        mobile: '',
        address: ''
      };
      $scope.addUser = function () {
        orderService.addUser($scope.addUserForm, $q)
          .then(function (res) {
            $ionicPopup.alert({
              title: 'Success',
              content: 'Retailer Added!!!'
            }).then(function (res) {
              $state.go('dashboard');
              console.log('Test Alert Box');
              console.log(res);
            });

          })
      };
      $scope.gotoDashboard = function () {
        $state.go('dashboard.retailers');
      };
      //         $scope.data = {
      //   showDelete: false
      // };

      // $scope.edit = function(item) {
      //   alert('Edit Item: ' + item.id);
      // };
      // $scope.share = function(item) {
      //   alert('Share Item: ' + item.id);
      // };

      // $scope.moveItem = function(item, fromIndex, toIndex) {
      //   $scope.items.splice(fromIndex, 1);
      //   $scope.items.splice(toIndex, 0, item);
      // };

      $scope.onItemDelete = function (item) {
        $scope.items.splice($scope.items.indexOf(item), 1);
      };

      $scope.items = [
        {id: 0},
        {id: 1},
        {id: 2},
        {id: 3},
        {id: 4},
        {id: 5},
        {id: 6},
        {id: 7},
        {id: 8},
        {id: 9},
        {id: 10}
      ];
      $scope.showActionsheet = function (item, itemIndex) {
        console.log(itemIndex);
        $ionicActionSheet.show({
          titleText: 'Action Options',
          buttons: [
            {text: '<i class="icon ion-ios-cart"></i> Place Order'},
            {text: '<i class="icon ion-more"></i> Customer Details'}
          ],
          destructiveText: '<i class="icon ion-trash-a"></i> Delete',
          cancelText: '<i class="icon"></i> Cencel',
          cancel: function () {
            console.log('CANCELLED');
          },

          buttonClicked: function (index) {
            if (index === 0) {
              console.log('Place Order', index);
              orderService.setRetailer(item);

              $state.go('order');

            }
            else {
              console.log('Customer Detail', index);
            }
            return true;
          },
          destructiveButtonClicked: function () {
            console.log('BUTTON CLICKED');

            var confirmPopup = $ionicPopup.confirm({
              title: 'Delete',
              template: 'Are you sure you want to delete this user?'
            });

            confirmPopup.then(function (res) {
              if (res) {
                console.log('You are sure');
              } else {
                console.log('You are not sure');
              }
            });
            // function(index) {

            // A confirm dialog


            return true;
          }
        });
      };

    })
})();
