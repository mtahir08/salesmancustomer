(function () {
  angular.module('myApp')
    .controller('confirmOrderController', function ($scope, $state, authService, $timeout, filterFilter, $ionicModal, $filter, orderService, $q) {
      $scope.allItems = [];
      $scope.category = [];
      $scope.itemsPrice = [];
      var abc = [];
      angular.forEach(authService.allItems, function (arrayItems, index) {
        angular.forEach(arrayItems, function (item) {
          $scope.allItems.push(item);
          if (filterFilter($scope.category, item.category) != item.category) {
            $scope.category.push(item.category);
            var obj = {
              category: item.category,
              price: item.price * item.quantity
            };
            $scope.itemsPrice.push(obj);
          } else {
            $scope.itemsPrice[$scope.category.indexOf(item.category)].price = $scope.itemsPrice[$scope.category.indexOf(item.category)].price + (item.price * item.quantity);
          }
        });
        if (authService.allItems.length == index)
          $scope.getTotalPrice();
      });
      $scope.category.sort();
      $scope.allItems = $filter('orderBy')($scope.allItems, 'category')
      $scope.itemsPrice = $filter('orderBy')($scope.itemsPrice, 'category');
      console.log($scope.itemsPrice);
  /*    $timeout(function () {
        $scope.itemsPrice
        $scope.allItems;
        $scope.category;
      }, 1);*/
      $scope.show = false;

      $scope.onItemDelete = function (item) {
        $scope.allItems.splice($scope.allItems.indexOf(item), 1);
        $scope.modal.hide();
      };

      $scope.getTotalPrice = function () {
        var i = 0;
        var totalPrice = 0;
        for (i; i < $scope.itemsPrice.length; i = i + 1) {
          totalPrice = totalPrice + $scope.itemsPrice[i].price;
        }
        return totalPrice;
      };

      $scope.orderConfirmed = function () {
        var arr = [];
        for (item in $scope.allItems) {
          arr.push({
            category: $scope.allItems[item].category,
            name: $scope.allItems[item].name,
            price: $scope.allItems[item].price,
            quantity: $scope.allItems[item].quantity
          })
        }
        orderService.order(arr, $scope.getTotalPrice(), $q)
      };


      //======================================
      //Modal starts
      //======================================
      $scope.item = {};
      var indexx = 0;
      var cat = '';
      $ionicModal.fromTemplateUrl('templates/modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.modal = modal;
      });

      $scope.updateItem = function (item) {
        $scope.allItems[$scope.allItems.indexOf(item)] = item;
        var p = filterFilter($scope.allItems, item.category);
        var temp = 0;
        for (var i = 0; i < p.length; i++) {
          temp += (p[i].price * p[i].quantity)
        }
        $scope.itemsPrice[$scope.category.indexOf(item.category)].price = temp;
        console.log($scope.itemsPrice);
        $scope.modal.hide();
      };
      $scope.showModal = function (item, index, c) {
        console.log(index, c, item);
        $scope.item = item;
        indexx = index;
        cat = c;
        $scope.modal.show()
      };

      $scope.increment = function () {
        $scope.item.quantity = $scope.item.quantity + 1;
      };
      $scope.decrement = function () {
        $scope.item.quantity <= 1 ? $scope.item.quantity = 1 : $scope.item.quantity -= 1;
      }


    })
})();
