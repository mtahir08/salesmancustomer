(function () {
  angular.module('myApp')
    .controller('orderController', function ($scope, $state, $filter, filterFilter, authService, $firebaseArray) {
      var companyRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey());
      var productsRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/products');
      var companyArr = $firebaseArray(companyRef);
      var productsArr = $firebaseArray(productsRef);
      $scope.categories = [];
      $scope.items = [];
      companyArr.$loaded().then(function () {
        $scope.allItems = authService.allItems;
        angular.forEach(productsArr, function (product) {
          console.log(product);
          if (filterFilter($scope.categories, product.category) != product.category) {
            console.log("in if");
            $scope.categories.push(product.category);
            $scope.items[$scope.categories.length - 1] = [];
            // console.log($scope.items[0].push(product))
            console.log($scope.categories.length - 1);
            $scope.items[$scope.categories.length - 1].push(product);
            $scope.category = $filter('orderBy')($scope.categories, 'product.category');
            $scope.items = $filter('orderBy')($scope.items, 'category');
            // $scope.category =$filter('orderBy')($scope.categories, 'product.category');
          } else {
            console.log("in else");
            $scope.items[$scope.categories.indexOf(product.category)].push(product);
          }
          console.log($scope.categories);
          console.log($scope.items);

        });
        // $scope.items = $firebaseArray(productsRef);

      });

      $scope.check = false;
      $scope.itemIndex = '';
      $scope.arrayIndex = '';
      $scope.count = 1;
      $scope.checkTab = function () {
        $scope.quantity = 1;
        console.log("counter");
      };
      $scope.resetCounter = function (tabIndex, category, check, count, arrayindex) {
        console.log($scope.items[arrayindex][tabIndex].checked);

        $scope.check == true && $scope.count > 1 ? $scope.items[arrayindex][tabIndex].quantity = 1 : $scope.items[arrayindex][tabIndex].quantity = count;
        $scope.items[arrayindex][tabIndex].checked = check;
        $scope.itemIndex = tabIndex;
        $scope.arrayIndex = arrayindex;
        $scope.count = 1;
        $scope.check = true
      };
      $scope.increment = function () {
        if ($scope.check == true && $scope.items[$scope.arrayIndex][$scope.itemIndex].checked == true) {
          $scope.items[$scope.arrayIndex][$scope.itemIndex].quantity = $scope.count = $scope.count + 1;
        }
        else {
          $scope.count = $scope.count + 1;
        }
      };
      $scope.decrement = function () {
        if ($scope.check == true && $scope.items[$scope.arrayIndex][$scope.itemIndex].checked == true) {
          //$scope.count <= 1 ? ($scope.items[$scope.arrayIndex][$scope.itemIndex].count = $scope.count = 1) : ($scope.items[$scope.arrayIndex][$scope.itemIndex].count = $scope.count =$scope.count - 1);
          $scope.items[$scope.arrayIndex][$scope.itemIndex].quantity = $scope.count = $scope.count - 1;
        }
        else {
          $scope.count = $scope.count - 1;
        }
      };
      $scope.goBack = function () {
        $state.go('dashboard');
      };
      $scope.allItems = [];
      $scope.orderNow = function () {
        var temp = [];
        angular.forEach($scope.items, function (item) {
          temp = filterFilter(item, checked = true);
          if (temp.length != 0)
            $scope.allItems.push(temp);
        });
        authService.allItems = $scope.allItems;
        $state.go('confirmOrder')
      }
    })
})();
