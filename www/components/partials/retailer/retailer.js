(function () {
  angular.module('myApp')
    .controller('retailerController', function ($scope, $state, $filter, filterFilter, authService, $stateParams, orderService) {
      console.log($stateParams.id);
      orderService.syncSalesmanObj.$loaded().then(function (data) {
        $scope.retailer = data.retailers[$stateParams.id];
      });
      $scope.gotoDashboard = function () {
        $state.go('dashboard.retailers');
      };
    })
})();
