(function () {
  angular.module('myApp')
    .controller('resetPasswordController', ResetPasswordController);
  function ResetPasswordController($scope, $http, $q, authService, $state) {
    $scope.resetPass = {
      password: '',
      cnfrmPass: ''
    };

    $scope.resetPassword = function () {
      authService.resetPassword($scope.resetPass.password)
        .then(function (data) {
          $state.go('login');
          console.log('data', data);
        },
        function (error) {
          console.log('error', error)
        })
    }
  }
})();
