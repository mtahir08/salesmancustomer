angular.module("myApp")
  // used self calling function to assign value in appConfig
  .constant("ApiEndpoint", (function () {

    var environment = "production";
    //var environment = "development";

    var config = {
      production: {
        'apiBaseUrl': 'https://salewhat.herokuapp.com/api/'
      },
      development: {
        'apiBaseUrl': 'http://localhost:3000/api/'
      }
    };
    return config[environment];
  })());
