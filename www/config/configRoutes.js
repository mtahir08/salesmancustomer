angular.module('myApp')
  .config(
  function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
    if (document.URL != 'https://salewhat.herokuapp.com/api/resetPassword') {
      $urlRouterProvider.otherwise("/login");
    }
    $stateProvider
      .state('login', {
        url: "/login",
        templateUrl: "../components/login/login.html",
        controller: 'loginController'
      })
      .state('forgot', {
        url: "/forgot",
        templateUrl: "../components/forgotPassword/forgotPassword.html",
        controller: "forgotPasswordController"
      })
      .state('resetPassword', {
        url: "/resetPassword",
        templateUrl: "../components/resetPassword/resetPassword.html",
        controller: "resetPasswordController"
      })
      .state('dashboard', {
        url: "/dashboard",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/dashboard/dashboard.html",
            controller: 'dashboardController'
          },
          "nav": {
            templateUrl: "../components/header/header.html",
            controller: 'headerController'
          }
        }
      })
      .state('dashboard.retailers', {
        url: "/retailers",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/retailers/retailers.html",
            controller: 'dashboardController'
          }
        }
      })
      .state('dashboard.retailer', {
        url: "/retailer/:id",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/partials/retailer/retailer.html",
            controller: 'retailerController'
          }
        }
      })
      .state('dashboard.orders', {
        url: "/orders",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/orders/orders.html",
            controller: 'dashboardController'
          }
        }
      })
      .state('profile', {
        url: "/profile",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/partials/profile/profile.html",
            controller: "profileController"
          },
          "nav": {
            templateUrl: "../components/header/header.html",
            controller: 'headerController'
          }
        }
      })
      .state('addRetailer', {
        url: "/addRetailer",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/partials/addRetailer/addRetailer.html",
            controller: 'addRetailerController'
          },
          "nav": {
            templateUrl: "../components/header/header.html",
            controller: 'headerController'
          }
        }
      })
      .state('order', {
        url: "/order",
        loginCompulsory: true,
        views: {
          "": {
            templateUrl: "../components/partials/order/order.html",
            controller: 'orderController'
          },
          "nav": {
            templateUrl: "../components/header/header.html",
            controller: 'headerController'
          }
        }
      })
      .state('confirmOrder', {
        url: "/confirmOrder",
        views: {
          "": {
            loginCompulsory: true,
            templateUrl: "../components/partials/confirmOrder/confirmOrder.html",
            controller: 'confirmOrderController'
          },
          "nav": {
            templateUrl: "../components/header/header.html",
            controller: 'headerController'
          }
        }
      })
  });
