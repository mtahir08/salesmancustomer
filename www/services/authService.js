(function () {
  angular.module('myApp')
    .service('authService', function ($http, $q, $state, ApiEndpoint) {
      var ref = firebase.database().ref();
      var isToken;
      var self = this;
      var companyKey, companyName;
      this.allItems = [];
      //var URL = "http://localhost:3000";
      this.genRef = firebase.database().ref('company' + companyKey);
      this.getToken = function () {
        return localStorage.getItem("token");
      };
      this.getUID = function () {
        return localStorage.getItem("uid");
      };
      this.setToken = function (token, uid) {
        localStorage.setItem("token", token);
        localStorage.setItem("uid", uid);
        isToken = true;
      };
      this.getIsToken = function () {
        return localStorage.getItem("token") ? true : false;
      };
      this.setIsToken = function (value) {
        localStorage.removeItem("token");
        isToken = false;
      };
      this.getCompany = function () {
        return companyName;
      };
      this.getCompanyKey = function () {
        if (localStorage.getItem("companyKey") == undefined) {
          // companyKey = localStorage.getItem("companyKey")
          var uid = localStorage.getItem("uid");
          ref.child("users").child(uid).on("value", function (snap) {
            // console.log(snap.val())
            companyName = snap.val().company;
            companyKey = snap.val().companyId;
            localStorage.setItem("companyKey", companyKey)
          });
          console.log("company key not found in localStorage");
          return companyKey;
        }
        return localStorage.getItem("companyKey");
      };
      this.setCompany = function (company) {
        // company.key ? companyKey = company.key : company.key
        companyKey = company.companyId;
        companyName = company.company;
        localStorage.setItem("companyKey", companyKey)
      };
      this.logIn = function (obj, $q) {
        var deferred = $q.defer();
        $http.post(ApiEndpoint.apiBaseUrl + 'login', obj)
          .success(function (data) {
            console.log('login data: ');
            console.log(data);
            // self.setCompany(data.obj.user);
            deferred.resolve(data);

            //$state.go('resetPassword');

//            console.log('login data: ' + JSON.stringify(data));
          })
          .error(function (data) {
            deferred.reject(data);
            //console.log('login Error: ' + JSON.stringify(data));
          });
        return deferred.promise;
      };
      this.forgotPassword = function (emailObj) {
        var deferred = $q.defer();
        $http.post(ApiEndpoint.apiBaseUrl + 'forgotPass', emailObj)
          .success(function (data) {
            deferred.resolve(data);
            console.log(' data: ' + JSON.stringify(data));
          })
          .error(function (data) {
            deferred.reject(data);
            console.log(' Error: ' + JSON.stringify(data));
          });
        return deferred.promise;
      };
      this.resetPassword = function (pass) {
        var deferred = $q.defer();
        var obj = {
          password: pass,
          oldPass: sessionStorage.getItem('tempPass'),
          uid: sessionStorage.getItem('uid'),
          email: sessionStorage.getItem('email')
        };
        $http.post(ApiEndpoint.apiBaseUrl + 'resetPass', obj)
          .success(function (responseData) {
            console.log(' responseData: ' + JSON.stringify(responseData));
            if (responseData.path === '/dashboard') {
              console.log('responseData', responseData);
              //window.location.assign("http://localhost:4000/#/dashboard");
              deferred.resolve(responseData);
            }
            else {
              console.log("error");
              deferred.reject();
            }
            //console.log(' responseData: ' + JSON.stringify(responseData));
          })
          .error(function (responseError) {
            deferred.reject(responseError);
            console.log(' Error: ' + JSON.stringify(responseError));
          });
        return deferred.promise;
      };
      this.logOut = function (obj, $q) {
        obj.uid = localStorage.getItem("uid");
        var deferred = $q.defer();
        $http.post(ApiEndpoint.apiBaseUrl + 'logout', obj)
          .success(function (data) {
            localStorage.removeItem("token");
            localStorage.removeItem("uid");
            isToken = false;
            deferred.resolve(data);
          })
          .error(function (data) {
            console.log("in error");
            deferred.reject(data);

          });
        return deferred.promise;
      }
    })
})();
