(function () {
  angular.module('myApp')
    .service('orderService', function ($http, $q, $state, ApiEndpoint, $firebaseArray, $firebaseObject, authService) {
      var Retailer = {};
      // companyId = localStorage.getItem('company');
      var self = this;
      // var orderRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/orders');
      self.companyRef = firebase.database().ref('products/' + authService.getCompanyKey());

      var orderRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/orders');
      // self.retailersRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/retailers');
      self.retailersRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/retailers');
      // var salesmanRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID());
      var salesmanRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID());
      // self.salesmanRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID());
      self.salesmanRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID());
      // self.salesmanOrderRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID() + '/orders');
      self.salesmanOrderRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID() + '/orders');
      // self.salesmanRetailersRef = new Firebase('https://salewhat.firebaseio.com/company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID() + '/retailers');
      self.salesmanRetailersRef = firebase.database().ref('company/' + authService.getCompanyKey() + '/salesmen/' + authService.getUID() + '/retailers');
      // var ref = new Firebase('https://salewhat.firebaseio.com/company');
      var ref = firebase.database().ref('company') ;

      var syncOrderArr = $firebaseArray(orderRef);
      var syncSalesmanArr = $firebaseArray(salesmanRef);
      self.syncSalesmanOrdersArr = $firebaseArray(self.salesmanOrderRef);
      self.syncSalesmanRetailersArr = $firebaseArray(self.salesmanRetailersRef);
      self.syncSalesmanArr = $firebaseArray(self.salesmanRef);
      self.syncSalesmanObj = $firebaseObject(self.salesmanRef);
      self.setRetailer = function (item) {
        Retailer = item;
      };
      self.getRetailer = function () {
        return Retailer
      };
      this.order = function (itemsArray, totalPrice, $q) {
        var deferred = $q.defer();
        console.log(itemsArray);
        var temp = self.getRetailer();
        var obj = {
          orderedItems: itemsArray,
          orderedAt: Firebase.ServerValue.TIMESTAMP,
          salesman: localStorage.getItem("uid"),
          totalPrice: totalPrice
        };
        console.log(obj);
        syncOrderArr.$loaded().then(function () {
          console.log(syncOrderArr);
          orderRef.child(temp.$id).update(obj).then(function (data, err) {
            if (err) {
              console.log('order Error: ' + JSON.stringify(data));
              deferred.reject(data);
            }
            else {
              syncSalesmanArr.$loaded().then(function () {
                salesmanRef.child('orders').child(temp.$id).update(obj).then(function (data, err) {
                  console.log('order data: ' + data);
                  deferred.resolve(data);
                  $state.go("dashboard")
                });
              })
            }
            return deferred.promise;
          });
        })
      };
      this.addUser = function (userObj, $q) {
        var deferred = $q.defer();
        userObj.createdAt = Firebase.ServerValue.TIMESTAMP;
        userObj.salesman = localStorage.getItem("uid");
        self.retailersRef = ref.child(authService.getCompanyKey()).child('retailers');
        console.log(self.retailersRef.toString());
        var Id = self.retailersRef.push();
        Id = Id.toString().split('/').pop();
        self.retailersRef.child(Id).update(userObj)
          .then(function (data, err) {
            if (err) {
              console.log('not successfully created: ' + err);
              deferred.reject(err);
            }
            else {
              var genRef = ref.child(authService.getCompanyKey()).child('salesmen').child(authService.getUID());
              genRef.child('retailers').child(Id).update(userObj);
              console.log(err + 'successfully created: ' + data);
              deferred.resolve(data);
            }
          });
        return deferred.promise;
      }
    })
})();
